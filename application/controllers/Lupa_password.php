<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lupa_password extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(['session','form_validation']);
		$this->load->helper('url');
		$this->load->model('lupa_password_model');
	}
   
     public function index()  
     {  
     	$this->load->view('lupapassword_view');
         
     }

     public function lupapassword(){
     	$email = $this->input->post('email');
     	
     	$result = $this->lupa_password_model->cek_email($email);
     			//cek hasil query
		if ($result->num_rows() > 0) {
			$data['data_user'] = $result->row();
			$code = uniqid(true);
			$data['token'] = $this->lupa_password_model->insert_token($code, $email);
			$this->load->library("phpmailer_library");
	        $mail = $this->phpmailer_library->load();

	        $mail->isSMTP();
	        $mail->Host     = 'smtp.gmail.com';
	        $mail->SMTPAuth = true;
	        $mail->Username = 'rara12148@gmail.com';
	        $mail->Password = 'paramartha';
	        $mail->SMTPSecure = 'ssl';
	        $mail->Port     = 465;
	        
	        $mail->setFrom('rara12148@gmail.com', 'CV. Tri Nata Teknikindo noreply');
	        // $mail->addReplyTo('info@example.com', 'CodexWorld');
	        
	        // Add a recipient
	        $mail->addAddress($email);
	        
	        // Add cc or bcc 
	 		/*$mail->addCC('cc@example.com');
	        $mail->addBCC('bcc@example.com');*/
	        
	        // Email subject
	        $mail->Subject = 'Reset Password Admin CV. Tri Nata Teknikindo';
	        
	        // Set email format to HTML 
	        $mail->isHTML(true);
	        
	        // Email body content
	        $mailContent = "<p>Please use the code to reset your password : </p><p><strong>$code</strong></p><p>If you did not request your password to be reset, ignore this message.<br>
	            <a href='index.php/lupa_password'>Lupa Password </a></p>
	            ";
	       	$mail->Body = $mailContent;
	        // Send email
	        if(!$mail->send()){
	            echo 'Message could not be sent.';
	            echo 'Mailer Error: ' . $mail->ErrorInfo;
	        }else{
	            // redirect(base_url('index.php/lupa_password/index_token'));
     			$this->load->view('token_view', $data);

	        }
		}
		else {
			$this->session->set_flashdata('error',"Email tidak ditemukan");
			redirect(base_url("index.php/Lupa_password", 'refresh'));
		}

     }
     public function index_passbaru(){
     	$id = $this->input->post('id');
     	$newpass = $this->input->post('newpass');
     	$konfirpass = $this->input->post('konfirpass');
	     	if($newpass == $konfirpass) {
				$this->lupa_password_model->update_password($id, md5($newpass));
				$this->session->set_flashdata('success',"password berhasil dirubah");
				redirect(base_url("index.php/user"));
			}
			else {
				$this->session->set_flashdata('error',"password baru tidak sama");
				redirect(base_url("index.php/lupa_password", 'refresh'));
			}

     }
     public function cek_token(){
     	$token = $this->input->post('token');
     	$id = $this->input->post('id');
     	$result = $this->lupa_password_model->cek_token($token, $id);
     			//cek hasil query
		if ($result->num_rows() > 0) {
			$data['data_user'] = $result->row();
			$this->load->view('passbaru_view', $data);
		}
		else {
			$this->session->set_flashdata('error',"password baru tidak sama");
			redirect(base_url("index.php/lupa_password", 'refresh'));
			
		}
     }
}
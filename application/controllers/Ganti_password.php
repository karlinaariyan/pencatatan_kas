<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganti_password extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model("ganti_password_model");

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}

	public function index()
	{

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Ganti_Password/Ganti_password_view');
		$this->load->view('footer_view');
	}

	public function update()
	{   
		$passwordlama=$this->input->post('passwordlama');
		$password1 = $this->input->post('password1');
		$password2 = $this->input->post('password2');

		$result = $this->ganti_password_model->cek_User($this->session->userdata('id'), md5($passwordlama));


		if ($result['valid_user']) {
			if($password1 == $password2) {
				$this->ganti_password_model->update_password($this->session->userdata('id'),md5($password2));

				$this->session->set_flashdata('success',"password berhasil dirubah");
				
				redirect(base_url("index.php/ganti_password"));
				
			}
			else {
				$this->session->set_flashdata('error',"password baru tidak sama");
				redirect(base_url("index.php/ganti_password"));
			}
		}
		else {
			//jika user tidak ditemukan di database
			$this->session->set_flashdata('error', 'Password yang anda masukkan salah');
				redirect(base_url("index.php/ganti_password"));
		}

	}

}

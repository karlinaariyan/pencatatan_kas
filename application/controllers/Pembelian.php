<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('pembelian_model');
		$this->load->library('session');
		
		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index()
	{
		// ambil data lawantransaksi
		$data_pembelian = $this->pembelian_model->ambil_semua_pembelian(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['pembelian'] = $data_pembelian;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/pembelian/data_pembelian', $data);
		$this->load->view('footer_view');
	}
	public function index_form()
	{
		// ambil data lawantransaksi
		$data_lawantransaksi= $this->pembelian_model->ambil_semua_lawantransaksi(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['lawantransaksi'] = $data_lawantransaksi;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/pembelian/tambah_pembelian', $data);
		$this->load->view('footer_view');
	}
	public function index_lap()
	{
		$data_pembelian = $this->pembelian_model->ambil_laporan_pembelian(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['pembelian'] = $data_pembelian;

		$this->load->view('content/Laporan/Laporan_Pembelian', $data);
	}
	public function index_lap_tgl()
	{
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Laporan/Filter_tanggal_pembelian.php');
		$this->load->view('footer_view');
	}
	public function index_semua_lap()
	{
		$data_pembelian = $this->pembelian_model->ambil_semua_laporan_pembelian(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['pembelian'] = $data_pembelian;

		$this->load->view('content/Laporan/Laporan_Pembelian', $data);
	}
	public function create()
	{
		$no_fak = $this->input->post('no_fak');
		$tanggal = $this->input->post('tanggal');
		$total= $this->input->post('total');
		$lawantransaksi = $this->input->post('lawantransaksi');
		$keterangan = $this->input->post('keterangan');

	 	$this->pembelian_model->tambah_pembelian($no_fak, $tanggal, $total, $lawantransaksi, $keterangan);
	
		$this->session->set_flashdata('success', 'Tambah pembelian berhasil');

		redirect('pembelian');
	}
	public function delete($id)
	{
		$this->pembelian_model->hapus_pembelian($id);

		$this->session->set_flashdata('success', "Hapus pembelian berhasil");

		redirect('pembelian');
	}
	public function edit($id= null)//handling error tanpa id
{ 
	//handling error tanpa id
	if(!isset($id)) redirect('pembelian');

	$data['pembelian'] = $this->pembelian_model->ambil_pembelian($id);
	$data['lawantransaksi'] = $this->pembelian_model->ambil_semua_lawantransaksi();
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/pembelian/edit_pembelian.php',$data); //content 
		$this->load->view('footer_view');
}

	public function update($id = null)
{
	if(!isset($id)) redirect('pembelian');

	 $this->pembelian_model->update_pembelian($id);
	
	$this->session->set_flashdata('success', "Edit pembelian berhasil");
			
	redirect('pembelian');
}
}



<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(['session','form_validation']);
		$this->load->helper('url');
		$this->load->model('user_model');
	}

	public function index()
	{
		if ($this->session->userdata('status_login') == true) {
			redirect(base_url('index.php/beranda'));
		}
		
		$data = array();
		$error = empty($this->session->flashdata('error')) ? '' : $this->session->flashdata('error');
		$data['error'] = $error;

		$this->load->view('login_view', $data);
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);

		//cek user di database melalui model
		$result = $this->user_model->cek_User($username, md5($password));

		if ($result['valid_user']) {
			$user = $result['data_user'];

			
			//buat array data user
			$data_session = array(
				'id' => $user->id_user,
				'nama' => $username,
				// 'status' => "login"
				'status_login' => true
				);

			//daftarkan array data user ke session
			$this->session->set_userdata($data_session);

			//arahkan user ke beranda
			redirect(base_url('index.php/beranda'));
		}
		else {
			//jika user tidak ditemukan di database
			$this->session->set_flashdata('error', 'Username dan password yang anda masukkan tidak ditemukan');
			redirect(base_url());
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		
		redirect(base_url());
	}
}	

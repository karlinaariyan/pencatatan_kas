<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('masuk_model');
		$this->load->library('session');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index()
	{
		// ambil data lawantransaksi
		$data_masuk = $this->masuk_model->ambil_semua_masuk(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['masuk'] = $data_masuk;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Transaksi_masuk/data_masuk', $data);
		$this->load->view('footer_view');
	}
	public function index_form()
	{
		// ambil data lawantransaksi
		$data_lawantransaksi= $this->masuk_model->ambil_semua_lawantransaksi(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['lawantransaksi'] = $data_lawantransaksi;

		// ambil data lawantransaksi
		$data_jeniskas= $this->masuk_model->ambil_semua_jeniskas(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['jeniskas'] = $data_jeniskas;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Transaksi_masuk/tambah_masuk', $data);
		$this->load->view('footer_view');
	}
	public function create()
	{
		$tanggal = $this->input->post('tanggal');
		$lawantransaksi= $this->input->post('lawantransaksi');
		$debit = $this->input->post('debit');
		$jeniskas = $this->input->post('jeniskas');
		$keterangan = $this->input->post('keterangan');

	 	$this->masuk_model->tambah_masuk( $tanggal, $lawantransaksi, $debit, $jeniskas, $keterangan);
	
		$this->session->set_flashdata('success', 'Tambah Transaksi Masuk berhasil');

		redirect('masuk');
	}
	public function delete($id)
	{
		$this->masuk_model->hapus_masuk($id);

		$this->session->set_flashdata('success', "Hapus Transaksi Masuk berhasil");

		redirect('masuk');
	}
	public function edit($id= null)//handling error tanpa id
{ 
	//handling error tanpa id
	if(!isset($id)) redirect('masuk');

	$data['masuk'] = $this->masuk_model->ambil_masuk($id);
	$data['lawantransaksi'] = $this->masuk_model->ambil_semua_lawantransaksi();
	$data['jeniskas'] = $this->masuk_model->ambil_semua_jeniskas(); 

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Transaksi_masuk/edit_masuk.php',$data); //content 
		$this->load->view('footer_view');
}

	public function update($id = null)
{
	if(!isset($id)) redirect('masuk');

	 $this->masuk_model->update_masuk($id);
	
	$this->session->set_flashdata('success', "Edit Transaksi Masuk berhasil");
			
	redirect('masuk');
}
}
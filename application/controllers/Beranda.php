<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url','session');
		$this->load->library('session');
		$this->load->model('beranda_model');
		$this->load->model('keluar_model');
		$this->load->model('masuk_model');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}

	public function index()
	{
		$tahun = date("Y");
		$tgl = date('Y-m-d');
		$param['masuk'] = $this->beranda_model->ambil_jumlah_masuk(); 
		$param['keluar'] = $this->beranda_model->ambil_jumlah_keluar();
		$param['penjualan'] = $this->beranda_model->ambil_jumlah_penjualan(); 
		$param['pembelian'] = $this->beranda_model->ambil_jumlah_pembelian();
		$param['keluarSaldo'] = $this->beranda_model->ambil_semua_keluar($tahun, $tgl);
		$param['masukSaldo'] = $this->beranda_model->ambil_semua_masuk($tahun, $tgl); 
		$data_beranda = $this->beranda_model->ambil_jumlah_transaksi_masuk($tahun, $tgl);
		$bulan = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		if ($data_beranda != null){
			foreach($data_beranda as $data){
              $month[] = $bulan[$data->month];
              $debit[] = (float) $data->debit;
              $kredit[] = (float) $data->kredit;
            }
		}
		else{
			$month[] = "";
            $debit[] = "";
            $kredit[] = "";
		} 

		$param["debit"] = $debit;
		$param["kredit"] = $kredit;
		$param["month"] = $month;


		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Dashboard/content_view',$param); //content
		$this->load->view('footer_view');
	}
}	
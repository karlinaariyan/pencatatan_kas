<?php

class Masuk_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_masuk()
	{

		//siapkan query builder
		$this->db->from('kas');
		$this->db->join('jeniskas', 'jeniskas.id_jeniskas = kas.id_jeniskas');
		$this->db->where('kredit', 0);

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
	public function ambil_semua_lawantransaksi()
	{
		return $this->db->get('lawantransaksi')->result();
		
	}
	public function ambil_semua_jeniskas()
	{
		return $this->db->get('jeniskas')->result();
		
	}
	public function tambah_masuk( $tanggal, $lawantransaksi, $debit, $jeniskas,$keterangan)
	{
		$this->tgl_kas = $tanggal;
		$this->npwp_lawantransaksi = $lawantransaksi;
		$this->debit = $debit;
		$this->id_jeniskas = $jeniskas;
		$this->uraian_kas = $keterangan;

		$this->db->insert('kas',$this);

		return $this->db->insert_id();
	}

	public function hapus_masuk($id)
	{
		$this->db->delete('kas', array('id_kas' => $id));
	}
	public function ambil_masuk($id)
	{
		return $this->db->get_where('kas', ['id_kas' => $id])->row();
		//maksudnya sama seperti = select * from masuk where id_masuk=$id
	}

	public function update_masuk($id)
	{
		//ambil inputan
		$tanggal = $this->input->post('tanggal');
		$lawantransaksi = $this->input->post('lawantransaksi');
		$debit = $this->input->post('debit');
		$jeniskas = $this->input->post('jeniskas');
		$keterangan = $this->input->post('keterangan');
		

		$this->tgl_kas = $tanggal;
		$this->npwp_lawantransaksi = $lawantransaksi;
		$this->debit = $debit;
		$this->id_jeniskas = $jeniskas;
		$this->uraian_kas = $keterangan;

		$this->db->update('kas', $this, ['id_kas' => $id]);
	}
}
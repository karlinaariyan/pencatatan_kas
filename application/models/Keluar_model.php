<?php

class Keluar_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_keluar()
	{

		//siapkan query builder
		$this->db->from('kas');
		$this->db->join('jeniskas', 'jeniskas.id_jeniskas = kas.id_jeniskas');
		$this->db->where('debit', 0);

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
	public function ambil_semua_lawantransaksi()
	{
		return $this->db->get('lawantransaksi')->result();
		
	}
	public function ambil_semua_jeniskas()
	{
		return $this->db->get('jeniskas')->result();
		
	}
	public function tambah_keluar( $tanggal, $lawantransaksi, $kredit, $jeniskas,$keterangan)
	{
		$this->tgl_kas = $tanggal;
		$this->npwp_lawantransaksi = $lawantransaksi;
		$this->kredit = $kredit;
		$this->id_jeniskas = $jeniskas;
		$this->uraian_kas = $keterangan;

		$this->db->insert('kas',$this);

		return $this->db->insert_id();
	}

	public function hapus_keluar($id)
	{
		$this->db->delete('kas', array('id_kas' => $id));
	}
	public function ambil_keluar($id)
	{
		return $this->db->get_where('kas', ['id_kas' => $id])->row();
		//maksudnya sama seperti = select * from keluar where id_keluar=$id
	}

	public function update_keluar($id)
	{
		//ambil inputan
		$tanggal = $this->input->post('tanggal');
		$lawantransaksi = $this->input->post('lawantransaksi');
		$kredit = $this->input->post('kredit');
		$jeniskas = $this->input->post('jeniskas');
		$keterangan = $this->input->post('keterangan');
		

		$this->tgl_kas = $tanggal;
		$this->npwp_lawantransaksi = $lawantransaksi;
		$this->kredit = $kredit;
		$this->id_jeniskas = $jeniskas;
		$this->uraian_kas = $keterangan;

		$this->db->update('kas', $this, ['id_kas' => $id]);
	}
}
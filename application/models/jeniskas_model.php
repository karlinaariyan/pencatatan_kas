<?php

class Jeniskas_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_jeniskas()
	{
		return $this->db->get('jeniskas')->result();
		
	}
	public function tambah_jeniskas($jeniskas, $keterangan)
	{
		$this->jeniskas = $jeniskas;
		$this->keterangan_jeniskas = $keterangan;

		$this->db->insert('jeniskas',$this);

		return $this->db->insert_id();
	}
	public function hapus_jeniskas($id)
	{
		$this->db->delete('jeniskas', array('id_jeniskas' => $id));
	}
	public function ambil_jeniskas($id)
	{
		return $this->db->get_where('jeniskas', ['id_jeniskas' => $id])->row();
		//maksudnya sama seperti = select * from jeniskas where id_jeniskas=$id
	}

	public function update_jeniskas($id)
	{
		//ambil inputan
		$jeniskas = $this->input->post('jeniskas');
		$keterangan = $this->input->post('keterangan');
		

		$this->jeniskas = $jeniskas;
		$this->keterangan_jeniskas = $keterangan;

		$this->db->update('jeniskas', $this, ['id_jeniskas' => $id]);
	}
}
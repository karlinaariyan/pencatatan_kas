<?php
class User_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function cek_user($username, $password)
	{
		$result = array();

		//siapkan query builder
		$this->db->from('users');
		$this->db->where('username', $username);
		$this->db->where('password', $password);

		//eksekusi query
		$query = $this->db->get();

		//cek hasil query
		if ($query->num_rows() > 0) {
			$result['valid_user'] = true;
			$result['data_user'] = $query->row();
		}
		else {
			$result['valid_user'] = false;
			$result['data_user'] = array();
		}

		return $result;
	}
}
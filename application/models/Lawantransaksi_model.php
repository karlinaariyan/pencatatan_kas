<?php

class Lawantransaksi_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_lawantransaksi()
	{
		return $this->db->get('lawantransaksi')->result();
		
	}
	public function tambah_lawantransaksi($npwp, $nama, $alamat, $keterangan)
	{
		$this->npwp = $npwp;
		$this->nama_lawantransaksi = $nama;
		$this->alamat = $alamat;
		$this->keterangan_lawantransaksi = $keterangan;

		$this->db->insert('lawantransaksi',$this);

		return $this->db->insert_id();
	}
	public function hapus_lawantransaksi($id)
	{
		$this->db->delete('lawantransaksi', array('id_lawantransaksi' => $id));
	}
	public function ambil_lawantransaksi($id)
	{
		return $this->db->get_where('lawantransaksi', ['id_lawantransaksi' => $id])->row();
		//maksudnya sama seperti = select * from lawantransaksi where id_lawantransaksi=$id
	}

	public function update_lawantransaksi($id)
	{
		//ambil inputan
		$npwp = $this->input->post('npwp');
		$nama= $this->input->post('nama');
		$alamat= $this->input->post('alamat');
		$keterangan= $this->input->post('keterangan');
		

		$this->npwp = $npwp;
		$this->nama_lawantransaksi = $nama;
		$this->alamat = $alamat;
		$this->keterangan_lawantransaksi = $keterangan;

		$this->db->update('lawantransaksi', $this, ['id_lawantransaksi' => $id]);
	}
}
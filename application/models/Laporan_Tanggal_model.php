<?php

class Laporan_Tanggal_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_laporan_tanggal($tglAwal, $tglAkhir)
	{

		//siapkan query builder
		$this->db->from('kas');
		$this->db->join('jeniskas', 'jeniskas.id_jeniskas = kas.id_jeniskas');
		$this->db->where('tgl_kas >=', $tglAwal);
		$this->db->where('tgl_kas <=', $tglAkhir);


		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
	public function ambil_semua_laporan()
	{

		//siapkan query builder
		$this->db->from('kas');
		$this->db->join('jeniskas', 'jeniskas.id_jeniskas = kas.id_jeniskas');

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
}
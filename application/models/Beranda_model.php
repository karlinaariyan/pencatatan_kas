<?php

class Beranda_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_jumlah_transaksi_masuk($tahun, $tgl)
	{
	$query = $this->db->query("SELECT MONTH(tgl_kas) as month, SUM(debit) AS debit, SUM(kredit) AS kredit FROM kas WHERE YEAR(tgl_kas) = '$tahun' and tgl_kas <= '$tgl' GROUP BY MONTH(tgl_kas)");

          
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
		
		
	}
}
	public function ambil_jumlah_saldo()
	{
		$this->db->select('SUM(debit)');
		$this->db->select('SUM(kredit)');
		$this->db->from('kas');

		return $this->db->get()->result();
	
/*	$query = $this->db->query("SELECT SUM(debit) AS debit, SUM(kredit) AS kredit FROM kas");

		return $query->result();
*/          
        
		
		
	}

		public function ambil_jumlah_masuk()
		{
		$query = $this->db->query("SELECT id_kas FROM kas where debit !=0 ");
			          
/*			 if($query->num_rows() > 0){
			       foreach($query->result() as $data){
			          $hasil[] = $data;
			     }
			  return $hasil;*/
			  return $query->num_rows();
		
		
	}
	public function ambil_jumlah_keluar()
		{
		$query = $this->db->query("SELECT id_kas FROM kas where kredit !=0 ");
			          
/*			 if($query->num_rows() > 0){
			       foreach($query->result() as $data){
			          $hasil[] = $data;
			     }
			  return $hasil;*/
			  return $query->num_rows();
		
		
	}

	public function ambil_jumlah_penjualan()
		{
		$query = $this->db->query("SELECT id_penjualan FROM penjualan");
			          
/*			 if($query->num_rows() > 0){
			       foreach($query->result() as $data){
			          $hasil[] = $data;
			     }
			  return $hasil;*/
			  return $query->num_rows();
		
		
	}
	public function ambil_jumlah_pembelian()
		{
		$query = $this->db->query("SELECT id_pembelian FROM pembelian");
			          
/*			 if($query->num_rows() > 0){
			       foreach($query->result() as $data){
			          $hasil[] = $data;
			     }
			  return $hasil;*/
			  return $query->num_rows();
		
		
	}
	public function ambil_semua_masuk($tahun, $tgl)
	{

		//siapkan query builder
		$this->db->from('kas');
		$this->db->join('jeniskas', 'jeniskas.id_jeniskas = kas.id_jeniskas');
		$this->db->where('kredit', 0);
		$this->db->where('YEAR(tgl_kas)', $tahun);
		$this->db->where('tgl_kas <=', $tgl);

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
	public function ambil_semua_keluar($tahun,$tgl)
	{

		//siapkan query builder
		$this->db->from('kas');
		$this->db->join('jeniskas', 'jeniskas.id_jeniskas = kas.id_jeniskas');
		$this->db->where('debit', 0);
		$this->db->where('YEAR(tgl_kas)', $tahun);
		$this->db->where('tgl_kas <=', $tgl);
		

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}

}
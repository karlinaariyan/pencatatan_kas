<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url('assets/img/tnt.jpeg'); ?>" type="image/jpeg" sizes="16x16">
	<title>Login Pencatatan Kas</title>
	 <!-- Font Awesome -->
 	 <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
	<!-- include bootstrap css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body style="height: 100vh;" background="<?php echo base_url('assets/img/background.jpg'); ?>">

	<div class="container h-100">
		<div class="row h-100 align-items-center">
			<div class="offset-md-4 col-md-4">
				<div class="card">

				<?php if($this->session->flashdata('success')) { ?>
    			<div class="alert alert-success" role="alert">
      				<?php echo $this->session->flashdata('success'); ?>
   				</div>
  				<?php } ?>

				  <div class="card-body">
				    <h4 class="card-title"><center><strong>Login Pencatatan Kas</strong></center></h4>
				    <div class="row">
				    	<img class="user-image" src="<?php echo base_url('assets/img/tnt.jpeg'); ?>">
				    </div>
				    <!-- tempatnya form -->
				    <form action="<?php echo base_url('index.php/user/login') ?>" method="POST">
				      <div class="form-group">
				        <label for="username">Username</label>
				        <input type="text" class="form-control" id="username" name="username" placeholder="Input username">
				      </div>
				      <div class="form-group">
				        <label for="password">Password</label>
				        <input type="password" class="form-control" id="password" name="password" placeholder="Input password">
				      </div>

				      <div class="form-group">
				        <input type="checkbox" onclick="showPass()"> <small>Show Password</small>
				        <script>
							function showPass() {
							  var x = document.getElementById("password");
							  if (x.type === "password") {
							    x.type = "text";
							  } else {
							    x.type = "password";
							  }
							}
						</script>
				      </div>
				      <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
				    </form>
				    <!-- end form -->

				    <!-- tampilkan flashdata (jika ada) -->
				    <?php if(!empty($error)) { ?>
				    <hr>
				    <div class="alert alert-danger" role="alert">
				    	<?php echo $error; ?>
				    </div>
					<?php } ?>

					<p>
						<a href="<?php echo base_url('index.php/lupa_password') ?>">Lupa Password ?</a>
					</p>

				  </div>
				</div>
			</div>
		</div>
	</div>

	<!-- include jquery -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>" />

	<!-- include bootstrap js -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" />
</body>
</html>
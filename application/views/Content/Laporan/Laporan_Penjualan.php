<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Laporan Penjualan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
    <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/jvectormap/jquery-jvectormap.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="<?php echo base_url('assets/font.css'); ?>">
</head>

<body>
	<center>
  <div class="row">
     <img src="<?php echo base_url('assets/img/penjualan.jpeg'); ?>">  
    </div>  
</center>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                     <div class="box">
            <div class="header">

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabel-data" class="table table-striped table-hover table-bordered table-sm">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Faktur</th>
                  <th>Harga</th>
                  <th>PPN</th>
                  <th>DPP</th>
                  <th>NPWP Lawan Transaksi</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $no = 0;
                    foreach ($penjualan as $item) {
                      $no++;
                  ?>
      
                <tr>
                  <td scope="row"><?php echo $no; ?></td>
                  <td><?php echo date('d M Y',strtotime($item->tgl_penjualan)); ?></td>
                  <td><?php echo $item->nofaktur_penjualan; ?></td>
                  <td><?php echo "Rp. " . number_format($item->totalharga_penjualan,2,',','.'); ?></td>
                  <td><?php echo "Rp. " . number_format($item->ppn,2,',','.'); ?></td>
                  <td><?php echo "Rp. " . number_format($item->dpp,2,',','.'); ?></td>
                  <td><?php echo $item->npwp; ?></td>
                  <td><?php echo $item->keterangan_penjualan; ?></td>
                </tr>
                
                 <?php
                }
                ?>


                </tbody>
       
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <button class="btn btn-primary" id="btnPrint" onclick="printLap()"><i class="fa fa-print"></i> Cetak </button>  
          <a href="<?php echo base_url('index.php/penjualan/index_lap_tgl'); ?>" class="btn btn-danger" id="btnBack"><i class="fa fa-arrow-left"></i> Kembali </a>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

	<script>
     var hidden = false;
    function printLap() {
      hidden = !hidden
      if (hidden){
         document.getElementById('btnPrint').style.visibility = 'hidden';
         document.getElementById('btnBack').style.visibility = 'hidden';
      }
     window.print();
     document.getElementById('btnPrint').style.visibility = 'visible';
     document.getElementById('btnBack').style.visibility = 'visible';

  }
   
  </script>
	
</body>
</html>
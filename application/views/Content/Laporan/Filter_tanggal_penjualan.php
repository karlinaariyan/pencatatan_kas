<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pembelian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Laporan Penjualan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('index.php/penjualan/index_lap'); ?>" method="post" enctype="multipart/form-data">

              <div class="box-body">
                 <div class="form-group">
                  <label for="tglawal" class="col-sm-2 control-label">Tanggal Awal</label>
                  <div class="col-sm-10"> 
                  <input type="date" class="form-control" id="tglawal" name="tglawal" placeholder="Tanggal">
                  </div> 
                </div>
          
                 <div class="form-group">
                  <label for="tglakhir" class="col-sm-2 control-label">Tanggal Akhir</label>
                  <div class="col-sm-10"> 
                  <input type="date" class="form-control" id="tglakhir" name="tglakhir" placeholder="Tanggal">
                  </div> 
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="<?php echo base_url('index.php/pembelian');?>" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Submit</button>
                <a href="<?php echo base_url('index.php/penjualan/index_semua_lap');?>" class="btn btn-success pull-right"><i class="fa fa-eye"></i> View All </a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Jenis Kas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><strong><font color=blue>Form Edit Jenis Kas</font></strong></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form class="form-horizontal" action="<?php echo base_url('index.php/jeniskas/update/'.$jeniskas->id_jeniskas); ?>" method="post">
              
            <div class="box-body">
                <div class="form-group">
                  <label for="jeniskas" class="col-sm-2 control-label">Jenis Kas</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="jeniskas" name="jeniskas" placeholder="Jenis Kas" value="<?php echo $jeniskas->jeniskas; ?>" onkeyup="allLetter(this)" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <textarea id="keterangan" name="keterangan" rows="10" cols="80" placeholder="Keterangan"><?php echo $jeniskas->keterangan_jeniskas; ?></textarea>
                  </div>
                </div>
                
  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="<?php echo base_url('index.php/jeniskas');?>" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

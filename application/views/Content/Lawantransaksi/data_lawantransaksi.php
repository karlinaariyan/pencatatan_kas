<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>

        <?php if($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Lawan Transaksi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                     <div class="box">
            <div class="box-header">
              <h3 class="box-title"><strong><font color=blue>Lawan Transaksi</font></strong></h3></h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="col-md-12">
                <a class="btn btn-info pull-right" href="<?php echo base_url('index.php/lawantransaksi/index_form');?>"><i class="fa fa-plus-square"></i> Tambah Lawan Transaksi</a>
              </div>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NPWP</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $no = 0;
                    foreach ($lawantransaksi as $item) {
                      $no++;
                  ?>
      
                <tr>
                  <td scope="row"><?php echo $no; ?></td>
                  <td><?php echo $item->npwp; ?></td>
                  <td><?php echo $item->nama_lawantransaksi; ?></td>
                  <td><?php echo $item->alamat; ?></td>
                  <td><?php echo $item->keterangan_lawantransaksi; ?></td>
                  <td>
                    <a href="<?php echo base_url('index.php/lawantransaksi/delete/'.$item->id_lawantransaksi) ?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin menghapus <?php echo $item->nama_lawantransaksi; ?> ? ')"> <i class="fa fa-trash-o"></i> Hapus</a>
                    <a href="<?php echo base_url('index.php/lawantransaksi/edit/'.$item->id_lawantransaksi) ?>" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i> Edit</a>
                  </td>
        
                </tr>
                <?php
                }
                ?>


                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

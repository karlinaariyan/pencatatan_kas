<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>

        <?php if($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pembelian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                     <div class="box">
            <div class="box-header">
              <h3 class="box-title"><strong><font color=blue>Pembelian</font></strong></h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="col-md-12">
                <a class="btn btn-info pull-right" href="<?php echo base_url('index.php/pembelian/index_form');?>"><i class="fa fa-plus-square"></i> Tambah Pembelian</a>
              </div>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Faktur</th>
                  <th>Harga</th>
                  <th>NPWP Lawan Transaksi</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $no = 0;
                    foreach ($pembelian as $item) {
                      $no++;
                  ?>
      
                <tr>
                  <td scope="row"><?php echo $no; ?></td>
                   <td><?php echo date('d M Y',strtotime($item->tgl_pembelian)); ?></td>
                  <td><?php echo $item->nofaktur_pembelian; ?></td>
                  <td><?php echo "Rp. " . number_format($item->totalharga_pembelian,2,',','.'); ?></td>
                  <td><?php echo $item->npwp; ?></td>
                  <td><?php echo $item->keterangan_pembelian; ?></td>
                  <td>
                    <a href="<?php echo base_url('index.php/pembelian/delete/'.$item->id_pembelian); ?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin menghapus <?php echo $item->nofaktur_pembelian; ?> ? ')"> <i class="fa fa-trash-o"></i> Hapus</a>
                    <a href="<?php echo base_url('index.php/pembelian/edit/'.$item->id_pembelian); ?>" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i> Edit</a>
                  </td>
        
                </tr>
                <?php
                }
                ?>


                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

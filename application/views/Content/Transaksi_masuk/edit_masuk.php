<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transaksi Masuk</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><strong><font color=blue>Form Edit Transaksi Masuk</font></strong></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form class="form-horizontal" action="<?php echo base_url('index.php/masuk/update/'.$masuk->id_kas); ?>" method="post">


              <div class="box-body">
                
                 <div class="form-group">
                  <label for="id_kas" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10"> 
                  <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" value="<?php echo $masuk->tgl_kas; ?>">
                  </div> 
                </div>

                  <div class="form-group">
                  <label for="lawantrans" class="col-sm-2 control-label">NPWP Lawan Transaksi</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="lawantransaksi" name="lawantransaksi">
                       <option value="">-- Silahkan Pilih Lawan Transaksi --</option>
                      <?php foreach($lawantransaksi as $items){ ?>
                        <?php if ($items->npwp == $masuk->npwp_lawantransaksi){?>
                       <option value="<?php echo $items->npwp; ?>" selected>
                          <?php echo $items->npwp. " - ". $items->nama_lawantransaksi; ?></option> 
                      <?php }
                      else {?>
                        <option value="<?php echo $items->npwp; ?>">
                          <?php echo $items->npwp. " - ". $items->nama_lawantransaksi; ?></option>
                    
                      
                        <?php }} ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="jeniskas" class="col-sm-2 control-label">Jenis Kas</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="jeniskas" name="jeniskas">
                      <?php foreach($jeniskas as $items){ ?>
                        <?php if ($items->id_jeniskas == $masuk->id_jeniskas){?>
                       <option value="<?php echo $items->id_jeniskas; ?>" selected>
                          <?php echo $items->jeniskas; ?></option> 
                      <?php }
                      else {?>
                        <option value="<?php echo $items->id_jeniskas; ?>">
                          <?php echo $items->jeniskas; ?></option>
                    
                      
                        <?php }} ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="uraian" class="col-sm-2 control-label">Uraian</label>
                  <div class="col-sm-10">
                    <textarea id="uraian" name="keterangan" rows="10" cols="80" placeholder="uraian"><?php echo $masuk->uraian_kas; ?></textarea>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="debit" class="col-sm-2 control-label">Debit</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="debit" name="debit" placeholder="Debit" value="<?php echo $masuk->debit; ?>" required>
                  </div>
                </div>

                
  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="<?php echo base_url('index.php/masuk');?>" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <center>Sistem Pencatatan Kas</center>
      </h1>
      <?php 
      if($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } else if ($this->session->flashdata('error')) {?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-times"></i> Error!</h4>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ganti Password</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Ganti Password </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('index.php/ganti_password/update'); ?>" method="post">

              <div class="box-body">
                 <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password Lama</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="passwordlama" name="passwordlama" placeholder="Password Lama" required>
                  </div>
              </div>

                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password Baru</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password1" name="password1" placeholder="Password Baru" required>
                  </div>
              </div>
                
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Konfirmasi Password Baru</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="Konfrmasi Password" required>
                  </div>
                </div>
                
  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href= "<?php echo base_url('index.php/beranda');?>" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save" disabled></i> Simpan</button>
              </div>
              <!-- /.box-footer -->

            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

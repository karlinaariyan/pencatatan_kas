<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	 <!-- Font Awesome -->
 	 <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
	<!-- include bootstrap css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body style="height: 100vh;" background="<?php echo base_url('assets/img/background.jpg'); ?>">

	<div class="container h-100">
		<div class="row h-100 align-items-center">
			<div class="offset-md-4 col-md-4">
				<div class="card">

				<?php if($this->session->flashdata('success')) { ?>
    			<div class="alert alert-success" role="alert">
      				<?php echo $this->session->flashdata('success'); ?>
   				</div>
  				<?php } ?>

				  <div class="card-body">
				    <h4 class="card-title"><center><strong>Masukkan Token</strong></center></h4>
				    <div class="row">
				    </div>
				    <!-- tempatnya form -->
				    <form action="<?php echo base_url('index.php/lupa_password/cek_token') ?>" method="POST">
				      <div class="form-group">
				        <label for="token">Masukkan token yang sesuai dengan yang dikirim di Email</label>
				        <input type="text" class="form-control" id="token" name="token" placeholder="Input Token">
				        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $data_user->id_user ?>">
				      </div>

				      <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in" aria-hidden="true"></i> Submit</button>
				    </form>
				    <!-- end f orm -->

				  </div>
				</div>
			</div>
		</div>
	</div>

	<!-- include jquery -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>" />

	<!-- include bootstrap js -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" />
</body>
</html>
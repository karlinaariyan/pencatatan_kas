-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2019 at 05:59 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tnt_kas`
--

-- --------------------------------------------------------

--
-- Table structure for table `jeniskas`
--

CREATE TABLE `jeniskas` (
  `id_jeniskas` int(11) NOT NULL,
  `jeniskas` varchar(100) NOT NULL,
  `keterangan_jeniskas` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jeniskas`
--

INSERT INTO `jeniskas` (`id_jeniskas`, `jeniskas`, `keterangan_jeniskas`) VALUES
(3, 'Gaji', '<p>Gaji untuk karyawan</p>\r\n'),
(4, 'Pengepakan', '<p>Bahan, bahan pendukung, alat kerja</p>\r\n'),
(5, 'Pembayaran Tagihan Listrik', '<p>Pembayaran lisrik per bulan</p>\r\n'),
(6, 'Telpon', '<p>Pembayaran Telpon</p>\r\n'),
(7, 'Saldo Awal Tahun', '<p>Jumlah saldo awal tahun</p>\r\n'),
(8, 'ATK', '<p>Alat Tulis Kantor</p>\r\n'),
(9, 'Biaya Kantor', '<p>fotokopi, benda pos, pemeliharaan kantor</p>\r\n'),
(10, 'Pengangkutan', '<p>BBM, Tol, Parkir</p>\r\n'),
(11, 'THR', '<p>THR untuk karyawan</p>\r\n'),
(12, 'Bonus', '<p>Bonus Karyawan</p>\r\n'),
(13, 'Air', '<p>Pembayaran tagihan air</p>\r\n'),
(14, 'BBM', '<p><tt>Terdiri atas pembelian bahan bakar, bayar tol, dan bayar parkir</tt></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id_kas` int(11) NOT NULL,
  `tgl_kas` date NOT NULL,
  `uraian_kas` varchar(500) NOT NULL,
  `debit` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `npwp_lawantransaksi` varchar(100) NOT NULL,
  `id_jeniskas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lawantransaksi`
--

CREATE TABLE `lawantransaksi` (
  `id_lawantransaksi` int(11) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `nama_lawantransaksi` varchar(200) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `keterangan_lawantransaksi` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lawantransaksi`
--

INSERT INTO `lawantransaksi` (`id_lawantransaksi`, `npwp`, `nama_lawantransaksi`, `alamat`, `keterangan_lawantransaksi`) VALUES
(1, '81.179.791.9-643.000', 'CV. SEJAHTERA', 'Juanda Harapan Permai', '<p>Lawan transaksi CV. SEJAHTERA</p>\r\n'),
(2, '021166316604000', 'CV DINAMIKA UTAMA JAYA', 'KENDUNG NO. 7, SURABAYA', '');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `nofaktur_pembelian` varchar(200) NOT NULL,
  `tgl_pembelian` datetime NOT NULL DEFAULT current_timestamp(),
  `totalharga_pembelian` varchar(200) NOT NULL,
  `id_lawantransaksi` varchar(100) NOT NULL,
  `keterangan_pembelian` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `nofaktur_penjualan` varchar(200) NOT NULL,
  `tgl_penjualan` date NOT NULL,
  `totalharga_penjualan` varchar(200) NOT NULL,
  `ppn` varchar(200) NOT NULL,
  `dpp` varchar(200) NOT NULL,
  `id_lawantransaksi` varchar(100) NOT NULL,
  `keterangan_penjualan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `token` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `nama`, `token`, `email`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500', 'admin', '15d4b755c62a09', 'rarawidya70@yahoo.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jeniskas`
--
ALTER TABLE `jeniskas`
  ADD PRIMARY KEY (`id_jeniskas`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id_kas`);

--
-- Indexes for table `lawantransaksi`
--
ALTER TABLE `lawantransaksi`
  ADD PRIMARY KEY (`id_lawantransaksi`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jeniskas`
--
ALTER TABLE `jeniskas`
  MODIFY `id_jeniskas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id_kas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `lawantransaksi`
--
ALTER TABLE `lawantransaksi`
  MODIFY `id_lawantransaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
